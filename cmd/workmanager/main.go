package main

import (
	"gitlab.com/fredericfok/example/config"
	"gitlab.com/fredericfok/example/pkg/worker"
	"gitlab.com/fredericfok/example/pkg/workerlog"
	"gitlab.com/fredericfok/example/pkg/workmanager"
)

func main() {

	workers := []workmanager.WorkerAgent{
		{Name: "task 1", Worker: worker.NewSimpleGoodWorker()},
		{Name: "task 2", Worker: worker.NewSimpleGoodWorker()},
		{Name: "task 3", Worker: worker.NewSimpleBadWorker()},
		{Name: "task 4", Worker: worker.NewComplexWorker(15)},
		{Name: "task 5", Worker: worker.NewComplexWorker(0)},
	}

	// Initialise our log system
	var workerLog = workerlog.NewLogger(config.LOG_OUTPUT)

	// Run workers a first time
	workerLog.Info("main", "Work manager First run")
	workManagerExample := workmanager.NewWorkManager(&workers, workerLog)
	workManagerExample.Run()
	workManagerExample.PrintLastResults()

	// Add a worker and run the workers again
	workerLog.Info("main", "Work manager Second run")
	workers = append(workers, workmanager.WorkerAgent{Name: "task 6", Worker: worker.NewComplexWorker(3)})
	workManagerExample.Run()
	workManagerExample.PrintLastResults()
}
