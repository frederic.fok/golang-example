OUTPUT_DIR := bin
BUILD := fredexample
VERSION := local

output_build := ${OUTPUT_DIR}/${BUILD}-${VERSION}

all-local: download tidy local-build-workmanager format local-test-unit local-report
all-dev: download tidy dev-build-workmanager format dev-test-unit dev-report

download:
	@printf -- "\033[37m |-> Download dependencies \033[0m \n"
	@printf -- "\033[34m $     go mod download \033[0m \n"
	@go mod download

tidy:
	@printf -- "\033[37m |-> Tidy dependencies \033[0m \n"
	@printf -- "\033[34m $     go mod tidy \033[0m \n"
	@go mod tidy

local-build-workmanager:
	@printf -- "\033[37m |-> Building the program \033[0m \n"
	@printf -- "\033[34m $     go build -tags local -o ${output_build} ./cmd/workmanager \033[0m \n"
	@go build -tags local -o ${output_build} ./cmd/workmanager

dev-build-workmanager:
	@printf -- "\033[37m |-> Building the program \033[0m \n"
	@printf -- "\033[34m $     go build -tags dev -o ${output_build}-dev ./cmd/workmanager \033[0m \n"
	@go build -tags dev -o ${output_build}-dev ./cmd/workmanager

format:
	@printf -- "\033[37m |-> Format code \033[0m \n"
	@printf -- "\033[34m $     go fmt ./... \033[0m \n"
	@go fmt ./... > formattedfiles.out

local-test-unit:
	@mkdir -p reports
	@printf -- "\033[37m |-> Run unit tests \033[0m \n"
	@printf -- "\033[34m $     go test -tags local -run TestUnit ./... -coverprofile=reports/coverage-local.out \033[0m \n"
	@go test -tags local -run TestUnit ./... -coverprofile=reports/coverage-local.out

dev-test-unit:
	@mkdir -p reports
	@printf -- "\033[37m |-> Run unit tests \033[0m \n"
	@printf -- "\033[34m $     go test -tags dev -run TestUnit ./... -coverprofile=reports/coverage-dev.out \033[0m \n"
	@go test -tags dev -run TestUnit ./... -coverprofile=reports/coverage-dev.out

local-report:
	@mkdir -p reports
	@printf -- "\033[37m |-> Find static code issues \033[0m \n"
	@printf -- "\033[34m $     go vet -json ./... \033[0m \n"
	@go vet -tags local -json ./...
	@printf -- "\033[37m |-> Generate Code Coverage Analysis \033[0m \n"
	@printf -- "\033[34m $     go tool cover -html=reports/coverage-local.out -o reports/coverage-local.html \033[0m \n"
	@go tool cover -html=reports/coverage-local.out -o reports/coverage-local.html

dev-report:
	@mkdir -p reports
	@printf -- "\033[37m |-> Find static code issues \033[0m \n"
	@printf -- "\033[34m $     go vet -tags dev -json ./... \033[0m \n"
	@go vet -tags dev -json ./...
	@printf -- "\033[37m |-> Generate Code Coverage Analysis \033[0m \n"
	@printf -- "\033[34m $     go tool cover -html=reports/coverage-dev.out -o reports/coverage-dev.html \033[0m \n"
	@go tool cover -html=reports/coverage-dev.out -o reports/coverage-dev.html
