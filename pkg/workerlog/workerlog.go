package workerlog

import (
	"github.com/sirupsen/logrus"
)

type WorkerLogger struct {
	*logrus.Logger
}

func NewLogger(log_output string) *WorkerLogger {
	var logger = logrus.New()
	var workerLogger = &WorkerLogger{logger}
	switch log_output {
	case "TTY":
		workerLogger.Formatter = &logrus.TextFormatter{}
	case "JSON":
		workerLogger.Formatter = &logrus.JSONFormatter{}
	default:
		workerLogger.Formatter = &logrus.JSONFormatter{}
	}

	return workerLogger
}

func (wl *WorkerLogger) Info(from string, data string) {
	wl.WithFields(logrus.Fields{
		"from": from,
	}).Info(data)
}

func (wl *WorkerLogger) WorkerInfo(from string, worker string, workerReportedTime string, data string) {
	wl.WithFields(logrus.Fields{
		"from":               from,
		"worker":             worker,
		"workerReportedTime": workerReportedTime,
	}).Info(data)
}
