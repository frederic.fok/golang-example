package workmanager

import (
	"testing"

	"gitlab.com/fredericfok/example/pkg/workerlog"
	"gotest.tools/assert"
)

func TestUnit_WorkManager_Run_GivenNumberOfWorkers_WhenTriggered_ShouldReturnEquivalentNumberOfResults(t *testing.T) {
	type fields struct {
		workers *[]WorkerAgent
		log     *workerlog.WorkerLogger
		want    int
	}
	tests := []struct {
		name   string
		fields fields
	}{
		// prepare
		{
			name: "one-worker",
			fields: fields{
				workers: &[]WorkerAgent{{Name: "task 1", Worker: NewWorkerMock()}},
				log:     workerlog.NewLogger("JSON"),
				want:    1,
			},
		},
		{
			name: "two-workers",
			fields: fields{
				workers: &[]WorkerAgent{{Name: "task 1", Worker: NewWorkerMock()}, {Name: "task 2", Worker: NewWorkerMock()}},
				log:     workerlog.NewLogger("JSON"),
				want:    2,
			},
		},
		{
			name: "three-workers",
			fields: fields{
				workers: &[]WorkerAgent{{Name: "task 1", Worker: NewWorkerMock()}, {Name: "task 2", Worker: NewWorkerMock()}, {Name: "task 3", Worker: NewWorkerMock()}},
				log:     workerlog.NewLogger("JSON"),
				want:    3,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// act
			workManager := NewWorkManager(tt.fields.workers, tt.fields.log)
			workManager.Run()

			// assert
			assert.Equal(t, tt.fields.want, len(workManager.result))
		})
	}
}

func TestUnit_WorkManager_Run_GivenNumberOfWorkers_WhenTriggeredTwice_ShouldReturnEquivalentNumberOfResultsForEachRun(t *testing.T) {
	type fields struct {
		workers            *[]WorkerAgent
		workerForSecondRun *WorkerAgent
		log                *workerlog.WorkerLogger
		wantFirstRun       int
		wantSecondRun      int
	}
	tests := []struct {
		name   string
		fields fields
	}{
		// prepare
		{
			name: "two-then-three-workers",
			fields: fields{
				workers:            &[]WorkerAgent{{Name: "task 1", Worker: NewWorkerMock()}, {Name: "task 2", Worker: NewWorkerMock()}},
				workerForSecondRun: &WorkerAgent{Name: "task 3", Worker: NewWorkerMock()},
				log:                workerlog.NewLogger("JSON"),
				wantFirstRun:       2,
				wantSecondRun:      3,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// act
			testWorkers := tt.fields.workers
			additionalTestWorker := tt.fields.workerForSecondRun
			workManager := NewWorkManager(testWorkers, tt.fields.log)
			workManager.Run()
			firstRunNumber := len(workManager.result)
			*testWorkers = append(*testWorkers, *additionalTestWorker)
			workManager.Run()
			secondRunNumber := len(workManager.result)

			// assert
			assert.Equal(t, tt.fields.wantFirstRun, firstRunNumber)
			assert.Equal(t, tt.fields.wantSecondRun, secondRunNumber)
		})
	}
}

type WorkerMock struct {
	jobProperty string
}

func NewWorkerMock() *WorkerMock {
	return &WorkerMock{"any-worker-property"}
}

func (worker *WorkerMock) Run(workName string) (string, error) {
	return "any-work-result", nil
}

func TestWorkManager_PrintLastResults_GivenNoWorker_WhenRun_ReturnZero(t *testing.T) {
	type fields struct {
		workers *[]WorkerAgent
		log     *workerlog.WorkerLogger
		result  []WorkerAgentResult
		want    int
	}
	tests := []struct {
		name   string
		fields fields
	}{
		// prepare
		{
			name: "at-least-one-worker",
			fields: fields{
				workers: &[]WorkerAgent{},
				log:     workerlog.NewLogger("JSON"),
				result:  []WorkerAgentResult{},
				want:    0,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// act
			testWorkers := tt.fields.workers
			workManager := NewWorkManager(testWorkers, tt.fields.log)
			workManager.Run()
			printed := workManager.PrintLastResults()

			// assert
			assert.Equal(t, tt.fields.want, printed)
		})
	}
}

func TestWorkManager_PrintLastResults_GivenAtLeastOneWorker_WhenNotRun_ReturnZero(t *testing.T) {
	type fields struct {
		workers *[]WorkerAgent
		log     *workerlog.WorkerLogger
		result  []WorkerAgentResult
		want    int
	}
	tests := []struct {
		name   string
		fields fields
	}{
		// prepare
		{
			name: "at-least-one-worker",
			fields: fields{
				workers: &[]WorkerAgent{{Name: "task 1", Worker: NewWorkerMock()}},
				log:     workerlog.NewLogger("JSON"),
				result:  []WorkerAgentResult{},
				want:    0,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// act
			testWorkers := tt.fields.workers
			workManager := NewWorkManager(testWorkers, tt.fields.log)
			workManager.Run()
			printed := workManager.PrintLastResults()

			// assert
			assert.Equal(t, tt.fields.want, printed)
		})
	}
}

func TestWorkManager_PrintLastResults_GivenTwoWorkers_WhenRun_ReturnTwo(t *testing.T) {
	type fields struct {
		workers *[]WorkerAgent
		log     *workerlog.WorkerLogger
		result  []WorkerAgentResult
		want    int
	}
	tests := []struct {
		name   string
		fields fields
	}{
		// prepare
		{
			name: "at-least-one-worker",
			fields: fields{
				workers: &[]WorkerAgent{{Name: "task 1", Worker: NewWorkerMock()}, {Name: "task 2", Worker: NewWorkerMock()}},
				log:     workerlog.NewLogger("JSON"),
				result:  []WorkerAgentResult{},
				want:    2,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// act
			testWorkers := tt.fields.workers
			workManager := NewWorkManager(testWorkers, tt.fields.log)
			workManager.Run()
			printed := workManager.PrintLastResults()

			// assert
			assert.Equal(t, tt.fields.want, printed)
		})
	}
}
