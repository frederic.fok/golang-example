package workmanager

import (
	"fmt"
	"sync"
	"time"

	"gitlab.com/fredericfok/example/pkg/worker"
	"gitlab.com/fredericfok/example/pkg/workerlog"
)

type WorkerAgent struct {
	Name   string
	Worker worker.WorkerIface
}

type WorkerAgentResult struct {
	Name   string
	Time   string
	Result string
}

type WorkManager struct {
	workers *[]WorkerAgent
	log     *workerlog.WorkerLogger
	result  []WorkerAgentResult
	mu      sync.Mutex
}

func NewWorkManager(workers *[]WorkerAgent, log *workerlog.WorkerLogger) *WorkManager {
	return &WorkManager{
		workers: workers,
		log:     log,
		result:  make([]WorkerAgentResult, 0, len(*workers)),
		mu:      sync.Mutex{},
	}
}

func (workManager *WorkManager) Run() {
	workManager.result = make([]WorkerAgentResult, 0, len(*workManager.workers))
	var wg sync.WaitGroup
	workManager.log.Info("workManager", fmt.Sprintf("number of jobs : %v \n", len(*workManager.workers)))

	for _, worker := range *workManager.workers {
		wg.Add(1)
		go func(w *sync.WaitGroup, workerAgent WorkerAgent, results *[]WorkerAgentResult) {
			defer w.Done()
			defer workManager.mu.Unlock()
			workResult, err := workerAgent.Worker.Run(workerAgent.Name)
			workManager.mu.Lock()
			if err != nil {
				*results = append(
					*results,
					WorkerAgentResult{
						workerAgent.Name,
						time.Now().String(),
						fmt.Sprintf("%s", err),
					})
			} else {
				*results = append(
					*results,
					WorkerAgentResult{
						workerAgent.Name,
						time.Now().String(),
						workResult,
					})
			}
		}(&wg, worker, &workManager.result)
	}
	wg.Wait()
}

func (workManager *WorkManager) PrintLastResults() int {
	printed := 0
	for _, r := range workManager.result {
		printed++
		workManager.log.WorkerInfo("workManager", r.Name, r.Time, r.Result)
	}
	return printed
}
