package worker

type WorkerIface interface {
	Run(workName string) (string, error)
}
