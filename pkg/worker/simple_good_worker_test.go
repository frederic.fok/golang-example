package worker

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUnit_SimpleGoodWorker_Run_GivenAGoodWorker_WhenRun_ReturnsAString(t *testing.T) {
	type fields struct {
		jobProperty string
	}
	type args struct {
		workName string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			name:    "job-returns-a-string",
			fields:  fields{"a-job-property"},
			args:    args{"a-worker-name"},
			want:    "my job is a-job-property",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			worker := &SimpleGoodWorker{
				jobProperty: tt.fields.jobProperty,
			}
			got, err := worker.Run(tt.args.workName)
			if (err != nil) != tt.wantErr {
				t.Errorf("SimpleGoodWorker.Run() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("SimpleGoodWorker.Run() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUnit_NewSimpleGoodWorker(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		{
			name: "worker-creation-with-hardcoded-default-job-property",
			want: "simply well done",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewSimpleGoodWorker()
			assert.Equal(t, tt.want, got.jobProperty)
		})
	}
}
