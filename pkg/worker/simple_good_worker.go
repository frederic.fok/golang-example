package worker

type SimpleGoodWorker struct {
	jobProperty string
}

func NewSimpleGoodWorker() *SimpleGoodWorker {
	return &SimpleGoodWorker{"simply well done"}
}

func (worker *SimpleGoodWorker) Run(workName string) (string, error) {
	return "my job is " + worker.jobProperty, nil
}
