package worker

import (
	"errors"
	"strconv"
	"strings"
)

// ComplexWorker is inherits SimpleGoodWorker through struct embodiment
type ComplexWorker struct {
	SimpleGoodWorker
	maxLoops int
}

func NewComplexWorker(maxLoops int) *ComplexWorker {
	return &ComplexWorker{SimpleGoodWorker{"complexly done"}, maxLoops}
}

func (worker *ComplexWorker) Run(workName string) (string, error) {
	var result []string
	fibo := justYetAnotherfibonacci()
	if worker.maxLoops <= 0 {
		return "my job is" + worker.jobProperty, errors.New("failing to process fibonaci so long as the number of loops is not greater than 0")
	} else {
		for i := 0; i < worker.maxLoops; i++ {
			result = append(result, strconv.Itoa(fibo()))
		}
		return "My job is " + worker.jobProperty + " as I have processed a fibonaci: " + strings.Join(result, " "), nil
	}
}

func justYetAnotherfibonacci() func() int {
	var a, b = 0, 1

	return func() int {
		defer func() {
			a, b = b, a+b
		}()
		return a
	}
}
