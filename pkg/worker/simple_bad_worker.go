package worker

import "errors"

type SimpleBadWorker struct {
	jobProperty string
}

func NewSimpleBadWorker() *SimpleBadWorker {
	return &SimpleBadWorker{jobProperty: "simply always failing"}
}

func (worker *SimpleBadWorker) Run(workName string) (string, error) {
	return "my job is" + worker.jobProperty, errors.New("my job is not done")
}
