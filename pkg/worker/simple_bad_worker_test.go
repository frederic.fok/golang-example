package worker

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUnit_SimpleBadWorker_Run_GivenABadWorker_WhenRun_ReturnsAlwaysAnError(t *testing.T) {
	type fields struct {
		jobProperty string
	}
	type args struct {
		workName string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:    "job-returns-an-error",
			fields:  fields{"any-job-property"},
			args:    args{"any-worker-name"},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			worker := &SimpleBadWorker{
				jobProperty: tt.fields.jobProperty,
			}
			_, err := worker.Run(tt.args.workName)
			assert.NotEqual(t, nil, err)
		})
	}
}
