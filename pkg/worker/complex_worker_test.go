package worker

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUnit_ComplexWorker_Run_GivenMaxLoopOfOne_ShouldReturnStringWithFiboNumbers(t *testing.T) {
	type fields struct {
		SimpleGoodWorker SimpleGoodWorker
		maxLoops         int
	}
	type args struct {
		workName string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		// that is mathematically arguable...
		{
			name:    "job-returns-a-complex-string-out-of-fibonaci",
			fields:  fields{SimpleGoodWorker: SimpleGoodWorker{"a-job-property"}, maxLoops: 1},
			args:    args{"a-worker-name"},
			want:    "My job is a-job-property as I have processed a fibonaci: 0",
			wantErr: false,
		},
		// that is arguably long...
		{
			name:    "job-returns-a-complex-string-out-of-fibonaci",
			fields:  fields{SimpleGoodWorker: SimpleGoodWorker{"a-job-property"}, maxLoops: 15},
			args:    args{"a-worker-name"},
			want:    "My job is a-job-property as I have processed a fibonaci: 0 1 1 2 3 5 8 13 21 34 55 89 144 233 377",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			worker := &ComplexWorker{
				SimpleGoodWorker: tt.fields.SimpleGoodWorker,
				maxLoops:         tt.fields.maxLoops,
			}
			got, err := worker.Run(tt.args.workName)
			if (err != nil) != tt.wantErr {
				t.Errorf("ComplexWorker.Run() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("ComplexWorker.Run() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUnit_ComplexWorker_Run_GivenMaxLoopNotGreaterThanZero_ShouldReturnAnError(t *testing.T) {
	type fields struct {
		SimpleGoodWorker SimpleGoodWorker
		maxLoops         int
	}
	type args struct {
		workName string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// Just an opiniated and contractual decision to raise an error if the maxLoop is null
		{
			name:    "job-returns-a-complex-string-out-of-fibonaci",
			fields:  fields{SimpleGoodWorker: SimpleGoodWorker{"a-job-property"}, maxLoops: 0},
			args:    args{"a-worker-name"},
			wantErr: true,
		},
		// that is arguably long...
		{
			name:    "job-returns-a-complex-string-out-of-fibonaci",
			fields:  fields{SimpleGoodWorker: SimpleGoodWorker{"a-job-property"}, maxLoops: -1},
			args:    args{"a-worker-name"},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			worker := &ComplexWorker{
				SimpleGoodWorker: tt.fields.SimpleGoodWorker,
				maxLoops:         tt.fields.maxLoops,
			}
			_, err := worker.Run(tt.args.workName)
			assert.NotEqual(t, nil, err)
		})
	}
}
