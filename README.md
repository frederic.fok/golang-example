# Golang Example Workspace

> Experimenting in Golang

## Example - worker manager

- the [workmanager example](https://gitlab.com/frederic.fok/golang-example/-/blob/master/cmd/workmanager/main.go) depicts usage of registering and running workers that will run concurrently
- the [workermanager](https://gitlab.com/frederic.fok/golang-example/-/blob/master/pkg/workmanager/workmanager.go) has its own package and is responsible for running all registered workers and awaiting for their respective results which it stores. The Worker manager does not persist results from all previous runs but rather makes available the result from the last run.
    - The workermanager will "wait" for the result of each worker (leveraging `waitgroup` and `mutex` enablers)
- all workers implement a common [worker interface](https://gitlab.com/frederic.fok/golang-example/-/blob/master/pkg/worker/worker_interface.go)
    - There can be different worker `Run` implementations
    - For instance a `sinmple` worker will simply return a string, and more `complex` worker will compute a little fibonnaci based on a maximum requested number
- Some mocks and tests are introduced to depicts testing usage within the packages, the structures and the interfaces
    - The ones depicted essentially mimic `table driven tests`, and mocking some dependencies. This can be challenged and/or complemented by other approaches (stubs, contract tests, integration tests)
- The use and extension of logrus in [workerlog](https://gitlab.com/frederic.fok/golang-example/-/blob/master/pkg/workerlog/workerlog.go) is merely added (as opposed to console output) to depict an example of structured logging which can be fine tuned. 

## Get Started

Generate a local build which output to tty, just for readability on the console output
```
make all-local
```

Generate a dev build which output in json format, much preferred for struct logging and further querying from a logging system
```
make all-dev
```

## Roadmap

- [ ] test cases surrounding the worker manager to assert each individual agent name has completed, and not only asserting the number of task
- [ ] declarative pipeline and cli commands to automate build (https://gitlab.com/public-k8s-references/pipelin.es/-/tree/master)
- [ ] incorporate precommit hooks
- [ ] incorporate gocyclo checks
- [ ] refine Makefile, build and flags for different underlying system
- [ ] possibly wraps this with pipelin.es cli or make its own urfave-based cli for triggering the example(s)?
- [ ] possibly incorporate a restful api to depict async usage for API consumers? and api / pact based testing approaches
- [ ] better config management, and potentially build time dependency injection with go wire and for compile type safety
- [ ] distribute the workers and make the work manager a grpc server
- [ ] may be some prometheus exporter for custom worker metrics :)
