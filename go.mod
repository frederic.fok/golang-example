module gitlab.com/fredericfok/example

go 1.15

require (
	github.com/google/go-cmp v0.5.5 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.2.2
	gotest.tools v2.2.0+incompatible
)
